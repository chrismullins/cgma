# CGMA: The Common Geometry Module, Argonne

This branch of [the canonical BitBucket repository](https://bitbucket.org/fathomteam/cgm)
contains fixes and support for the [CMake build system](http://cmake.org).
For information on CGMA, see [the Trac website/wiki](http://trac.mcs.anl.gov/projects/ITAPS/wiki/CGM).

# CMake specifics

The `CMakeLists.txt` files in each source directory can be used as an
alternative to the autoconf `Makefile.am` files for some configurations.
Specifically, the CMake scripts do not yet support parallel/MPI builds
of CGMA, nor or all of the test programs built.
